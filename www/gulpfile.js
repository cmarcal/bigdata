var gulp = require('gulp');
var watch = require('gulp-watch');
var cssmin = require("gulp-cssmin");
var concat = require("gulp-concat");
var stripCssComments = require('gulp-strip-css-comments');
var jsonminify = require('gulp-jsonminify');
var browserSync = require('browser-sync').create();
var inject = require('gulp-inject');
var debug = require ('gulp-debug');

var css = [
  'assets/css/card.css',
  'assets/css/cart.css',
  'assets/css/menu.css'
];

var json = [
  'assets/js/livros.json'
];

gulp.task('js', function () {
    return gulp.src('js/*js')
        .pipe(browserify())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// Start Deploy
gulp.task('minify-css', function(){
    gulp.src(css)
    .pipe(concat('style.min.css'))
    .pipe(stripCssComments({all: true}))
    .pipe(cssmin())
    .pipe(gulp.dest('assets/build/css/'));
});

gulp.task('minify-json', function(){
  gulp.src(json)
  .pipe(concat('cidades.min.json'))
  .pipe(jsonminify())
  .pipe(gulp.dest('assets/build/json/'));
});

gulp.task('watch', function() {
  gulp.watch(css,json ,  ['minify-css', 'minify-json']);
});

gulp.task('minfy-index', function () {
  var target = gulp.src('index.html');
  var sources = gulp.src(['assets/build/css/*.min.css']).pipe(debug());

  return target.pipe(inject(sources), {relative: true})
    .pipe(gulp.dest('./'));
});

gulp.task('deploy', [ 'minify-css', 'minify-json', 'minfy-index']);
// End Deploy

// Start Devs
gulp.task('index', function () {
  var target = gulp.src('index.html');
  var sources = gulp.src(['assets/css/*.css']).pipe(debug());

  return target.pipe(inject(sources), {relative: true})
    .pipe(gulp.dest('./'));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
              baseDir: "./"
          }
    });

    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', [ 'index', 'browser-sync']);
// End Dev
