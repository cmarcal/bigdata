bigdata 
    .factory('$localStorage', ['$window', function ($window) {
        return {
            prefix: 'welancer.',

            set: function (key, value) {
                $window.localStorage[this.prefix + key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[this.prefix + key] || defaultValue;
            },
            remove: function (key) {
                return $window.localStorage.removeItem(this.prefix + key);
            },
            setObject: function (key, value) {
                $window.localStorage[this.prefix + key] = JSON.stringify(value);
            },
            getObject: function (key, defaultValue) {
                return JSON.parse($window.localStorage[this.prefix + key] || defaultValue);
            },
            clear: function () {
                $window.localStorage.clear();
            }
        }
    }])
;
