var bigdata = angular.module('bigdata', ['ui.router'])

.config(['$stateProvider','$urlRouterProvider', '$locationProvider', '$qProvider', function ($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {
$qProvider.errorOnUnhandledRejections(false);
    $stateProvider
    .state('list', {
        url: '/list',
        controller: 'HomeController',
        templateUrl: 'templates/list.html?rdn=' + Date.now()
    })
    .state('cart', {
        url: '/cart',
        controller: 'cartController',
        templateUrl: 'templates/cart.html?rdn=' + Date.now()
    })

      $urlRouterProvider.otherwise('/list');

}]);

bigdata.run(['$rootScope', '$location', '$state', '$localStorage', '$sce', '$http', function($rootScope, $location, $state, $localStorage){

    $rootScope.showLoader = function(){
        $('.loader').show();
    }

    $rootScope.hideLoader = function(){
        $('.loader').hide();
    }

    console.log('Projeto iniciando...');
}]);
