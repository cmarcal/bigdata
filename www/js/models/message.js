welancer.factory('Message', function ($rootScope, $timeout, $localStorage, WDLChat) {

    return {

        //Lista todos as mensagens de um determinado canal
        getMessages: function (project_id, designer_id, cliente_id) {
            return WDLChat.getMessages(project_id, designer_id, cliente_id).then(function (messages) {
                return messages;
            });
        },
        
        //Lista todos os profissionais disponíveis de determinado projeto
        getProfessionals: function (user_id, project_id) {
            return WDLChat.getProfessionals(user_id, project_id).then(function (professionals) {
                return professionals;
            });
        },

        //Recupera um usuário pelo ID
        getUserById: function (user_id) {
            return WDLChat.getUserById(user_id).then(function (user) {
                return user;
            });
        },

        //Envia mensagem para determinado usuário
        sendMessage: function (project_id, sender_id, receiver_id, msg) {
            return WDLChat.sendMessage(project_id, sender_id, receiver_id, msg).then(function (msg) {
                return msg;
            });
        },

        //Recupera um projeto pelo ID
        getProjectById: function (user_id) {
            return WDLChat.getProjectById(user_id).then(function (user) {
                return user;
            });
        },
    };
});