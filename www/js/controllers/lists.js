bigdata.controller  ('HomeController' , ['$rootScope', '$scope', '$state', '$state' ,'$location', '$stateParams', '$window' ,"$localStorage" ,"$http", function ($rootScope, $scope, $state, $state ,$location, $stateParams, $window, $localStorage, $http){

    let urlbooks = 'assets/js/livros.json';
    
    $scope.booksList  = [];

    $http.get(urlbooks)
    .then (function(resp){
      $rootScope.jsonbooks = resp.data;
    });

    $scope.selectBooks = function(item) {

        if (!item.selected) {
            $scope.books += item.value;
             $scope.booksList.push(item);
             console.log($scope.booksList);
        } else {
            $scope.books -= item.value;
            $scope.booksList.splice($scope.booksList.indexOf(item), 1);
            console.log('removendo', $scope.booksList);
        }

        item.selected ? item.selected = false : item.selected = true;

    }

    $scope.salvarbooks = function () {
        var listaObj = $scope.booksList;
        var myStr = JSON.stringify(listaObj);
        $localStorage.setObject('lista', myStr);

        console.log($scope.booksList);
        $state.go('cart');

    }


}])
