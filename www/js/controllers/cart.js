bigdata.controller  ('cartController' , ['$rootScope', '$scope', '$state', '$state' ,'$location', '$stateParams', '$window' ,"$localStorage" ,"$http", function ($rootScope, $scope, $state, $state ,$location, $stateParams, $window, $localStorage, $http){

    $scope.getList = function() {
        $scope.myObj = JSON.parse($localStorage.getObject('lista' ,null));
        console.log($scope.myObj);

        $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.myObj.length; i++){
                var product = $scope.myObj[i];
                total += product.price;
            }
            return total;
        }
    }

    $scope.removeItem = function(item){
        $scope.myObj.splice($scope.myObj.indexOf(item), 1);
    }

    var init = function (){
        $scope.getList();
    }

    init();

}])
